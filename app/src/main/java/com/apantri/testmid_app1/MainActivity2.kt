package com.apantri.testmid_app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView


class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val name = intent.getStringExtra("Name")
        val nameString: TextView = findViewById<TextView>(R.id.ShowName)
        nameString.text = name
        val code = intent.getStringExtra("CODE")
        Log.d("Name", name.toString())
        Log.d("Code", code.toString())
    }
}
