package com.apantri.testmid_app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    var btn1: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn1 = findViewById<Button>(R.id.helloBtn)

        val name: TextView = findViewById(R.id.textName)
        val nameString: String = name.text.toString()
        val code: TextView = findViewById(R.id.text_stu_id)
        val codeString: String = code.text.toString()
        btn1 = findViewById<Button>(R.id.helloBtn)

        btn1!!.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
            intent.putExtra("Name", nameString)
            intent.putExtra("CODE", codeString)
            startActivity(intent)
        }
    }
}